var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var person = new Schema({
    castType: {type: String, default: "none"},
    crewType: {type: String, default: "none"},
    name: String,
    playing: String,
    picLocate: String,
    href: String,
    description: String,
});

module.exports = mongoose.model('Person', person);