var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;


var picture = new Schema({
    altText: String,
    picLoc: String,
    size: {type: Number, default: 7},
    // people: [{type: Object, ref: 'Person'}],
    location: String
});

module.exports = mongoose.model('Picture', picture);