var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var db = mongoose.connect('mongodb://localhost/koko');

var Person = require('./model/person');
var Picture = require('./model/picture');

//Allow all requests from all domains & localhost
app.all('/*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "POST, GET");
    next();
  });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// People
app.get('/person', function(req, res) {
    Person.find({}, function(err, persons) {
        if (err) {
            res.status(500).send({error: "Could not fetch person"});
        } else {
            res.send(persons);
        }
    });
});
app.post('/person', function(req, res) {
    var person = new Person();
    person.castType = req.body.castType;
    person.crewType = req.body.crewType;
    person.name = req.body.name;
    person.playing = req.body.playing;
    person.picLoc = req.body.picLoc;
    person.description = req.body.description;
    person.save(function(err, savedPerson) {
        if (err) {
            res.status(500).send({error:"Could not save person"});
        } else {
            res.send(savedPerson);
        }
    });
});

// Pictures
app.get('/picture', function(req, res) {
    Picture.find({}, function(err, pictures) {
        if (err) {
            res.status(500).send({error:"Could not find picture"});
        } else {
            res.send(pictures);
        }
    });
});
app.post('/picture', function(req, res) {
    var picture = new Picture();
    picture.altText = req.body.altText;
    picture.picLoc = req.body.picLoc;
    picture.size = req.body.size;
    // picture.people = req.body.people;
    picture.location = req.body.location;
    picture.save(function(err, savedPicture) {
        if (err) {
            res.status(500).send({error:"Could not save picture"});
        } else {
            res.send(savedPicture);
        }
    });
});

// app.put('/picture/person/add', function(req, res) {
//     Person.findOne({_id: req.body.personId}, function(err, person) {
//         if (err) {
//             res.status(500).send({error:"Could not add to person -1"});
//         } else {
//             Picture.update({_id:req.body.pictureId}, {$addToSet:{persons: person._id}}, function(err, picture) {
//                 if (err) {
//                     res.status(500).send({error:"Could not add to person -2"});
//                 } else {
//                     res.send("added to person, let hope");
//                 }
//             });
//         } 
//     })
// });

app.listen(3004, function() {
    console.log("Koko is running on port 3004");   
});