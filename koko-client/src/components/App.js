import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
// import Header from './Header';
import MainNav from './MainNav';
import Home from './pages/Home';
import Trailers from './pages/Trailers';
import PhotoGallery from './pages/PhotoGallery';
import Credits from './pages/Credits';
import About from './pages/About';
import Contact from './pages/Contact';


class App extends Component {
  render() {
    return (
      <div className="">
        <BrowserRouter>
          <MainNav />
          <Route path="/" exact component={Home} />
          <Route path="/trailers" exact component={Trailers} />
          <Route path="/photogallery" exact component={PhotoGallery} />
          <Route path="/credits" exact component={Credits} />
          <Route path="/about" exact component={About} />
          <Route path="/contact" exact component={Contact} />
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
