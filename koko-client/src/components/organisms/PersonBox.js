import React, { Component } from 'react';


class PersonBox extends Component {
    render() {
        return (
            <div className=" color-box">
                <img className="img-contain__staring-home" src={this.props.picLocate} alt={this.props.name} />
                <div className> <br />
                    <p className="actor">{this.props.name}</p>
                    <p className="playing">as {this.props.playing}</p>
                    <a href={this.props.name}>more info</a>
                </div>
            </div>
        );
    }
};

export default PersonBox;