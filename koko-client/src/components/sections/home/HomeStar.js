import React, { Component } from 'react';
import PersonBox from '../../organisms/PersonBox';
import HttpService from '../../../services/http-services';

const http = new HttpService();

class HomeStar extends Component {
    constructor(props) {
        super(props);
        this.state = {persons:[]};

        // bind functions
        this.loadData = this.loadData.bind(this);
        this.personList =this.personList.bind(this);
        // call functions
        this.loadData();

    }
    loadData = () => {
        var self = this;
        http.getPersons().then(data => {
            self.setState({persons: data}) 
        }, err => {
            
        });
    }
    personList = () => {
        const list = this.state.persons.map((personJSON) => 
            <div className="" key={personJSON._id}>
            
                <PersonBox castType={personJSON.castType} 
                            crewType={personJSON.crewType} 
                            name={personJSON.name}  
                            playing={personJSON.playing} 
                            picLocate={personJSON.picLocate} 
                            href={personJSON.href} 
                            description={personJSON.description} />
            </div>
         );
        return (list);
    }
    render() {
        return (
            <div>
                <article className="section__koko center-text">
                <h2 className="section-headers">Starring</h2> <br /> <br />
                    <div className="grid-3 wrap" >
                        {this.personList()}
                        {/* <PersonBox name="Nicole" playing="Zia" /> */}
                    </div>
                </article>
            </div>
        );
    }
}

export default HomeStar;