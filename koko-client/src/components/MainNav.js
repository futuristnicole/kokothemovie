import React from 'react';
import { Link } from 'react-router-dom';


const MainNav = () => {
    return (
        <div className="navigation">
        <input type="checkbox" className="navigation__checkbox" id="navi-toggle" />
        <label htmlFor="navi-toggle" className="navigation__button">
          <span className="navigation__icon">&nbsp;</span>
        </label>
        <div className="navigation__background">&nbsp;</div>
        <nav className="navigation__nav">
          <ul className="navigation__list">
            <li className="navigation__item"><Link to="/" className="navigation__link">Home</Link></li>
            <li className="navigation__item"><Link to="/trailers" className="navigation__link">Trailers</Link></li>
            <li className="navigation__item"><Link to="/photogallery" className="navigation__link">Photo Gallery</Link></li>
            <li className="navigation__item"><Link to="/credits" className="navigation__link">Credits</Link></li>
            <li className="navigation__item"><Link to="/about" className="navigation__link">About</Link></li>
            <li className="navigation__item"><Link to="/contact" className="navigation__link">Contact</Link></li>
          </ul>
        </nav>
      </div> 
    );
};

export default MainNav;
