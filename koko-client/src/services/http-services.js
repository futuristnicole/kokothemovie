import 'whatwg-fetch';

class HttpService { 
    getPersons = () => {
        var promise = new Promise((resolve, reject) => {
            fetch('http://localhost:3004/person').then(res => {
            resolve(res.json());
            })
        });
        return promise;
    } 
}

export default HttpService;